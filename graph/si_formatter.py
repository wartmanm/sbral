import math
import matplotlib.ticker as ticker

prefixes = {
    15: "P",
    12: "T",
    9: "G",
    6: "M",
    3: "k",
    0: "",
    -3: "m",
    -6: "μ",
    -9: "n",
    -12: "p"
}

def get_prefix(value):
    if value == 0:
        digits = 1
    else:
        digits = math.log10(abs(value))
    triplets = 3 * math.floor(digits / 3)
    triplets = min(max(triplets, -12), 15)
    prefix = prefixes[triplets]
    return (prefix, 10 ** -triplets)


class SIFormatter(ticker.Formatter):
    def __init__(self, unit, is_suffix = False):
        self.unit = unit
        self.is_suffix = is_suffix

    def __call__(self, x, pos=None):
        prefix, adjustment = get_prefix(x)
        adjusted = round(x * adjustment, 1)
        formatted = str(int(adjusted)) if adjusted == int(adjusted) else str(adjusted)

        if self.unit == '' or self.unit is None:
            return f"{formatted}{prefix}"
        elif self.is_suffix:
            return f"{formatted}{prefix} {self.unit}"
        else:
            return f"{formatted} {prefix}{self.unit}"
