#!/usr/bin/env python3
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import json
import re
import math
from si_formatter import SIFormatter, get_prefix

type_names = {
    "sbral": "sbral::SkewList",
    "im_vector": "im_rc::Vector",
    "vec": "std::vec::Vec",
    "dogged_vector": "dogged::DVec",
    "rpds_vector": "rpds::Vector",
}

baseline = "new"
root = "../target/criterion"
estimate_unit = 10 ** -9
benchmarks = [
    "append_1000",
    "lookup_first",
    "lookup_last",
    "lookup_scattered",
    "lookup_small",
    "update_first",
    "update_last",
    "update_scattered",
    "update_small"
]

log_benchmarks = ["build", "clone", "drop"]

max_vec_size = 100_000
short_vec_benchmarks = [
    "append_1000",
    "update_first",
    "update_last",
    "update_scattered",
]

type_colors = dict(zip(type_names.keys(), mpl.rc_params()["axes.prop_cycle"].by_key()["color"]))

def include_estimate(bench_type, benchmark, parameter_value):
    # These benchmarks would take prohibitively long with Vec, so arbitrary results are output instead
    if bench_type == "vec" and benchmark in short_vec_benchmarks and parameter_value > max_vec_size:
        return False
    return True

def read_estimates(bench_type, benchmark, time_scale):
    bench_dir = os.path.join(root, benchmark, bench_type)
    if not os.path.exists(bench_dir):
        return []
    values = []
    for parameter in [x for x in os.listdir(bench_dir) if re.match("^[\\d_,.]+$", x) is not None]:
        parameter_value = int(parameter.replace("_", ""))
        if not include_estimate(bench_type, benchmark, parameter_value):
            print(f"skipping {bench_type}/{benchmark}/{parameter_value}")
            continue

        estimate_path = os.path.join(bench_dir, parameter, baseline, "estimates.json")
        with open(estimate_path) as estimate_file:
            estimate = json.load(estimate_file)
        values.append((parameter_value, estimate["Mean"]["point_estimate"] * time_scale))
    values.sort()
    return values

def print_estimates(benchmark, all_estimates):
    if len(all_estimates) == 0:
        return
    print(f"{benchmark}:")
    for name, (x,y) in all_estimates.items():
        print(f"\t{name}:")
        for (xval, yval) in zip(x, y):
            print(f"\t\t{xval}: {yval}")

def read_all_estimates(bench_types, benchmark, time_scale):
    all_estimates = {}
    for bench_type in bench_types:
        estimates = read_estimates(bench_type, benchmark, time_scale)
        if len(estimates) > 0:
            x,y = zip(*estimates)
            all_estimates[bench_type] = (x,y)
    print_estimates(benchmark, all_estimates)
    return all_estimates

def format_plot(ax, benchmark):
    ax.set_title(f"{benchmark}: Comparison")
    ax.set_ylabel("Average time (μs)")
    ax.set_xlabel("Size")
    ax.legend(loc="upper left")
    ax.set_xscale('symlog', linthreshx=10.0)
    ax.xaxis.set_major_formatter(SIFormatter("", True))
    ax.grid()

def plot_benchmark(benchmark, time_scale):
    all_estimates = read_all_estimates(type_names.keys(), benchmark, time_scale)

    if len(all_estimates) > 0:
        fig, ax = plt.subplots()
        for name, (x,y) in all_estimates.items():
            full_name = type_names[name]
            ax.plot(x, y, marker="o", label=full_name, color=type_colors[name])

        format_plot(ax, benchmark)
        return fig, ax

def plot_logscale(benchmark):
    figax = plot_benchmark(benchmark, 10 ** -9)
    if figax is None:
        return
    fig, ax = figax
    ax.set_ylabel("Average time")
    ax.set_yscale("log")
    ax.yaxis.set_major_formatter(SIFormatter("s", False))
    plt.savefig(f"{benchmark}.svg")
    plt.close()

def plot_linear(benchmark):
    figax = plot_benchmark(benchmark, 10 ** -3)
    if figax is None:
        return
    fig, ax = figax
    plt.savefig(f"{benchmark}.svg")
    plt.close()

for bench in log_benchmarks:
    plot_logscale(bench)

for bench in benchmarks:
    plot_linear(bench)

