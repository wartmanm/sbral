#[macro_use]
extern crate criterion;

use sbral::SkewList;

use std::any::Any;
use std::cell::{Ref, RefCell};
use std::fmt::{self, Debug};
use std::iter::{Extend, FromIterator};
use std::mem;
use std::ops::{Index, IndexMut, Range};
use std::time::Duration;

use criterion::{
    AxisScale, BatchSize, Bencher, Criterion, ParameterizedBenchmark, PlotConfiguration,
};

use crate::constants::*;

use rand::seq::SliceRandom;
use rand::{Rng, SeedableRng};
use rand_pcg::Pcg32;

use dogged::DVec as DoggedVector;
use im_rc::vector::Vector as ImVector;
use rpds::Vector as RpdsVector;

// Other crates tested or not, and why:
// im: It's the most popular persistent datastructure crate by a factor of 100 or so.  It would be
// weird *not* to include it.  Vector is the closest analogue to sbral.
// rpds: List doesn't implement Index or IndexMut, but Vector does.
// dogged: It has at least one dependent crate, but it's another Clojure vector
// rust-immutable-seq: It's a finger tree! And it was last updated in early 2017 :(  It also gets
// into an infinite recursion in lookup_first/10000.  That's not ideal.
// immutable: Hasn't been updated since 2016, no documentation, doesn't have an immutable array
// anyway
// fingertree: Not updated since 2014, documentation link is dead.
// fingertrees: Another finger tree, updated late 2018.  Unfortunately it doesn't implement Index
// or IndexMut and it's not obvious how to implement them from outside.
// blist: It has a couple of dependents, and it's a different data structure.  But it doesn't
// implement Index or IndexMut either.

// Used only for sbral push/pop benchmarks
mod constants {
    pub const NODE_SIZE: usize = 16;
    pub const FANOUT: usize = 32;
    pub const fn increment_tree_size(i: usize) -> usize {
        i * FANOUT + NODE_SIZE
    }
}

// DVec doesn't implement FromIterator
#[derive(Debug, Clone)]
struct DVecWrapper<T>(DoggedVector<T>);
impl<T: Clone + Debug> FromIterator<T> for DVecWrapper<T> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = T>,
    {
        let mut dvec = DoggedVector::new();
        for i in iter {
            dvec.push(i);
        }
        DVecWrapper(dvec)
    }
}
impl<T: Clone + Debug> Index<usize> for DVecWrapper<T> {
    type Output = T;
    fn index(&self, index: usize) -> &T {
        &self.0[index]
    }
}
impl<T: Clone + Debug> IndexMut<usize> for DVecWrapper<T> {
    fn index_mut(&mut self, index: usize) -> &mut T {
        &mut self.0[index]
    }
}

#[rustfmt::skip]
static SIZES: &[usize] = &[
              0,
             10,
             25,
             50,
            100,
            250,
          1_000,
         10_000,
        100_000,
      1_000_000,
     10_000_000,
    100_000_000,
];

static MAX_BUILDABLE_SIZE: usize = 100_000;

fn sizes() -> impl Iterator<Item = usize> {
    SIZES.iter().cloned()
}

fn buildable_sizes() -> impl Iterator<Item = usize> {
    sizes().filter(|&size| size <= MAX_BUILDABLE_SIZE)
}

// Criterion doesn't have a --exact option.  Since all the sizes are prefixes of larger sizes,
// we have to add a unique character at the end to make it possible to run only certain sizes.
struct TrailingSlash<T: Debug>(pub T);
impl<T: Debug> Debug for TrailingSlash<T> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}/", self.0)
    }
}

struct CollectionCache {
    size: usize,
    list: RefCell<Box<dyn Any>>,
}

impl CollectionCache {
    fn get<T>(&self) -> Ref<T>
    where
        T: FromIterator<usize> + 'static,
    {
        let mut mut_list = self.list.borrow_mut();
        if !mut_list.is::<T>() {
            *mut_list = Box::new((0..self.size).collect::<T>());
        }
        mem::drop(mut_list);
        Ref::map(self.list.borrow(), |list| {
            let item: Option<&T> = list.downcast_ref::<T>();
            item.unwrap()
        })
    }

    fn new(size: usize) -> Self {
        CollectionCache {
            size: size,
            list: RefCell::new(Box::new(())),
        }
    }
}

impl Debug for CollectionCache {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}/", self.size)
    }
}

struct IndexCache(CollectionCache, Vec<usize>);

impl Debug for IndexCache {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self.0)
    }
}

fn append(c: &mut Criterion) {
    c.bench(
        "append_1000",
        ParameterizedBenchmark::new(
            "sbral",
            append_bench::<SkewList<usize>>,
            sizes().map(CollectionCache::new),
        )
        .with_function("im_vector", append_bench::<ImVector<usize>>)
        .with_function("rpds_vector", append_bench::<RpdsVector<usize>>)
        .with_function("dogged_vector", append_bench::<ImVector<usize>>)
        .with_function("vec", move |b, collections| {
            if collections.size > MAX_BUILDABLE_SIZE {
                b.iter_custom(|i| Duration::from_secs(60 * i));
            } else {
                b.iter_batched_ref(
                    move || {
                        let mut vec = collections.get::<Vec<usize>>().clone();
                        vec.reserve(vec.len() + 1000);
                        vec
                    },
                    |vec| vec.extend(0..1000usize),
                    // SmallInput and even LargeInput cause me to run out of memory :(
                    BatchSize::NumBatches(10_000),
                );
            }
        })
        .plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic)),
    );

    fn append_bench<T>(b: &mut Bencher, collections: &CollectionCache)
    where
        T: FromIterator<usize> + Clone + Extend<usize> + 'static,
    {
        let list = collections.get::<T>();
        let list_ref: &T = &*list;
        b.iter_batched_ref(
            || list_ref.clone(),
            |list: &mut T| {
                list.extend(0..1000usize);
            },
            BatchSize::SmallInput,
        );
    }
}

fn build(c: &mut Criterion) {
    c.bench(
        "build",
        ParameterizedBenchmark::new(
            "sbral",
            build_internal::<SkewList<_>>,
            buildable_sizes().map(TrailingSlash),
        )
        .with_function("im_vector", build_internal::<ImVector<_>>)
        .with_function("rpds_vector", build_internal::<RpdsVector<_>>)
        .with_function("dogged_vector", build_internal::<DVecWrapper<_>>)
        .with_function("vec", build_internal::<Vec<_>>)
        .plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic)),
    );

    fn build_internal<T>(b: &mut Bencher, &TrailingSlash(elems): &TrailingSlash<usize>)
    where
        T: FromIterator<usize>,
    {
        b.iter_batched_ref(
            || None,
            |holder| {
                let list: T = (0..elems).collect();
                *holder = Some(list);
            },
            BatchSize::SmallInput,
        );
    }
}

fn get_indexes(mut indexes: Vec<usize>) -> Vec<usize> {
    let mut rng = Pcg32::seed_from_u64(12345);
    indexes.shuffle(&mut rng);
    indexes
}

fn start_range(size: usize) -> Option<Vec<usize>> {
    match size {
        0 => None,
        _ if size > 1000 => Some((0..1000).into_iter().collect()),
        _ => Some((0..size).into_iter().cycle().take(1000).collect()),
    }
}

fn end_range(size: usize) -> Option<Vec<usize>> {
    match size {
        0 => None,
        _ if size > 1000 => Some(((size - 1000)..size).collect()),
        _ => Some((0..size).into_iter().cycle().take(1000).collect()),
    }
}

fn lookup_first(c: &mut Criterion) {
    lookup_range(c, "lookup_first", &start_range);
}

fn lookup_last(c: &mut Criterion) {
    lookup_range(c, "lookup_last", &end_range);
}

fn lookup_scattered(c: &mut Criterion) {
    lookup_indexes(c, "lookup_scattered", &|size| {
        get_scattered_indexes(size, 1000)
    });
}

fn get_collection_indexes<'a>(
    index_fn: &'a dyn Fn(usize) -> Option<Vec<usize>>,
) -> impl Iterator<Item = IndexCache> + 'a {
    sizes().flat_map(move |size| {
        index_fn(size).map(|indexes| IndexCache(CollectionCache::new(size), indexes))
    })
}

// Returns `count` indexes evenly distributed over the collection's range. We don't want to bias
// towards the start or back because that would unfairly help or hinder sbral.  It's probably not
// realistic for values to be so spread out, but the _first and _last benchmarks already measure
// tightly clustered values.
fn get_scattered_indexes(collection_size: usize, count: usize) -> Option<Vec<usize>> {
    if count == 0 || collection_size < count {
        return None;
    }
    let region_size = collection_size / count;
    let mut rng = Pcg32::seed_from_u64(12345);
    let mut indexes: Vec<_> = (0..collection_size)
        .step_by(region_size)
        .map(|offset| rng.gen_range(0, region_size) + offset)
        .collect();
    indexes.shuffle(&mut rng);
    Some(indexes)
}

fn lookup_indexes(c: &mut Criterion, name: &str, index_fn: &dyn Fn(usize) -> Option<Vec<usize>>) {
    c.bench(
        name,
        ParameterizedBenchmark::new(
            "sbral",
            lookup_indexes_internal::<SkewList<usize>>,
            get_collection_indexes(index_fn),
        )
        .with_function("im_vector", lookup_indexes_internal::<ImVector<usize>>)
        .with_function("rpds_vector", lookup_indexes_internal::<RpdsVector<usize>>)
        .with_function(
            "dogged_vector",
            lookup_indexes_internal::<DVecWrapper<usize>>,
        )
        .with_function("vec", lookup_indexes_internal::<Vec<usize>>)
        .plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic)),
    );

    fn lookup_indexes_internal<T>(b: &mut Bencher, collections: &IndexCache)
    where
        T: FromIterator<usize> + Index<usize, Output = usize> + 'static,
    {
        let list = collections.0.get::<T>();
        b.iter(move || {
            let sum: usize = collections
                .1
                .iter()
                .cloned()
                .fold(0, |accum, idx| accum.wrapping_add(list[idx]));
            sum
        });
    }
}

fn lookup_range(c: &mut Criterion, name: &str, range_fn: &dyn Fn(usize) -> Option<Vec<usize>>) {
    lookup_indexes(c, name, &move |size| range_fn(size).map(get_indexes));
}

fn update_first(c: &mut Criterion) {
    update_range(c, "update_first", &start_range);
}

fn update_last(c: &mut Criterion) {
    update_range(c, "update_last", &end_range);
}

fn update_scattered(c: &mut Criterion) {
    update_indexes(c, "update_scattered", &|size| {
        get_scattered_indexes(size, 1000)
    });
}

fn update_range(c: &mut Criterion, name: &str, range_fn: &dyn Fn(usize) -> Option<Vec<usize>>) {
    update_indexes(c, name, &move |size| range_fn(size).map(get_indexes));
}

fn update_indexes(c: &mut Criterion, name: &str, index_fn: &dyn Fn(usize) -> Option<Vec<usize>>) {
    c.bench(
        name,
        ParameterizedBenchmark::new(
            "sbral",
            update_indexes_internal::<SkewList<usize>>,
            get_collection_indexes(index_fn),
        )
        .with_function("im_vector", update_indexes_internal::<ImVector<usize>>)
        .with_function("rpds_vector", update_indexes_internal::<RpdsVector<usize>>)
        .with_function(
            "dogged_vector",
            update_indexes_internal::<DVecWrapper<usize>>,
        )
        .with_function("vec", move |b, collections| {
            if collections.0.size > MAX_BUILDABLE_SIZE {
                b.iter_custom(|i| Duration::from_secs(60 * i));
            } else {
                update_indexes_internal::<Vec<usize>>(b, collections);
            }
        })
        .plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic)),
    );

    fn update_indexes_internal<T>(b: &mut Bencher, collections: &IndexCache)
    where
        T: FromIterator<usize> + Clone + IndexMut<usize, Output = usize> + 'static,
    {
        let original_list = collections.0.get::<T>();
        b.iter_batched_ref(
            move || original_list.clone(),
            move |list| {
                for i in collections.1.iter().cloned() {
                    let tmp = &mut list[i];
                    *tmp = 1;
                }
            },
            BatchSize::SmallInput,
        );
    }
}

fn clone(c: &mut Criterion) {
    c.bench(
        "clone",
        ParameterizedBenchmark::new(
            "sbral",
            clone_internal::<SkewList<_>>,
            buildable_sizes().map(CollectionCache::new),
        )
        .with_function("im_vector", clone_internal::<ImVector<_>>)
        .with_function("rpds_vector", clone_internal::<RpdsVector<_>>)
        .with_function("dogged_vector", clone_internal::<DVecWrapper<_>>)
        .with_function("vec", move |b, collections| {
            if collections.size > MAX_BUILDABLE_SIZE {
                b.iter_custom(|i| Duration::from_secs(60 * i));
            } else {
                clone_internal::<Vec<_>>(b, collections);
            }
        })
        .plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic)),
    );
    fn clone_internal<T>(b: &mut Bencher, collections: &CollectionCache)
    where
        T: FromIterator<usize> + Clone + 'static,
    {
        let list_ref = collections.get::<T>();
        let list: &T = &*list_ref;
        b.iter_batched_ref(move || (), move |_| list.clone(), BatchSize::SmallInput);
    }
}

fn drop(c: &mut Criterion) {
    c.bench(
        "drop",
        ParameterizedBenchmark::new(
            "sbral",
            drop_internal::<SkewList<_>>,
            buildable_sizes().map(TrailingSlash),
        )
        .with_function("im_vector", drop_internal::<ImVector<_>>)
        .with_function("rpds_vector", drop_internal::<RpdsVector<_>>)
        .with_function("dogged_vector", drop_internal::<DVecWrapper<_>>)
        .with_function("vec", move |b, elems| {
            if elems.0 > MAX_BUILDABLE_SIZE {
                b.iter_custom(|i| Duration::from_secs(60 * i));
            } else {
                drop_internal::<Vec<_>>(b, elems);
            }
        })
        .plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic)),
    );

    fn drop_internal<T>(b: &mut Bencher, &TrailingSlash(elems): &TrailingSlash<usize>)
    where
        T: FromIterator<usize>,
    {
        b.iter_batched(
            move || (0..elems).collect::<T>(),
            mem::drop,
            BatchSize::SmallInput,
        )
    }
}

// This group of tests is intended to motivate adding push/pop amortization like in im.
// The slowest is upgrade_into_existing, at about 170ns vs 10ns for buffer-only.  The commonest
// is with_ones_place, at around 80ns.
// (a) In absolute terms that's still not that much
// (b) Is this sufficiently motivated for a persistent data structure?  The application this is
// intended for, for example, only pushes elements and sometimes reverts to an earlier version.
// (c) Is this sufficiently motivated for a data structure with O(1) push/pop?  There is no danger
// of repeatedly triggering a normally-amortized process, because those don't exist.
// (d) Can we afford it?  My initial implementation doubled the cost of appending to a non-full
// buffer, the commonest case, and regressed build() times by 40%.
// It will also increase the cost of clone() (since the back buffer will always be full if you only
// append, and all of its elements will need to be cloned) or else increase API complexity (a
// clone_mut() or commit() method to call before cloning?)
// This may be worth revisiting after switching to larger heap-allocated chunks.
fn sbral_push_pop_non_chunk_boundary(c: &mut Criterion) {
    sbral_push_pop(c, "push_pop_non_chunk_boundary", NODE_SIZE + 1);
}
fn sbral_push_pop_with_ones_place(c: &mut Criterion) {
    sbral_push_pop(c, "push_pop_with_ones_place", NODE_SIZE);
}
fn sbral_push_pop_upgrade_in_place(c: &mut Criterion) {
    // fill ones place, and buffer
    sbral_push_pop(
        c,
        "push_pop_upgrade_in_place",
        NODE_SIZE * FANOUT + NODE_SIZE,
    );
}
fn sbral_push_pop_no_ones_place(c: &mut Criterion) {
    // we want tail to be the twos place, so push/pop will need to make ones from scratch
    sbral_push_pop(
        c,
        "push_pop_no_ones_place",
        increment_tree_size(NODE_SIZE) + NODE_SIZE,
    );
}
fn sbral_push_pop_upgrade_into_existing(c: &mut Criterion) {
    // fill twos place, ones place, and buffer
    sbral_push_pop(
        c,
        "push_pop_upgrade_into_existing",
        increment_tree_size(NODE_SIZE) + NODE_SIZE * FANOUT + NODE_SIZE,
    );
}

fn sbral_push_pop(c: &mut Criterion, name: &str, start_len: usize) {
    c.bench_function(name, move |c| {
        let mut list: SkewList<usize> = (0..start_len).collect();
        c.iter(move || {
            // Because we are lazy about pushing full buffers into the tail, and pulling them back
            // when the buffer is empty, we have to push and pop twice.

            // Buffer is full, push it to tail so 1 can be added
            list.push(1);
            // Remove 1
            list.pop();
            // Buffer is empty, pull from tail so a value can be popped
            list.pop();
            // Restore buffer to full
            list.push(0);
        });
    });
}

criterion_group! {
    name = push_pop;
    config = Criterion::default();
    targets =
        sbral_push_pop_non_chunk_boundary,
        sbral_push_pop_with_ones_place,
        sbral_push_pop_upgrade_in_place,
        sbral_push_pop_no_ones_place,
        sbral_push_pop_upgrade_into_existing
}

criterion_group! {
    name = bench;
    config = Criterion::default();
    targets =
        build,
        append,
        lookup_first,
        lookup_last,
        lookup_scattered,
        update_first,
        update_last,
        update_scattered,
        clone,
        drop
}
criterion_main!(bench);
