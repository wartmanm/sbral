use crate::constants::*;
use arrayvec::{self, ArrayVec};
use std::ops::{Index, IndexMut};
use std::slice;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct Chunk<T> {
    value: ArrayVec<NodeValues<T>>,
}
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FullChunk<T> {
    value: Ref<NodeValues<T>>,
}

impl<T> Chunk<T>
where
    T: Clone,
{
    pub fn get_mut(&mut self, idx: usize) -> Option<&mut T> {
        self.value.get_mut(idx)
    }
    pub fn push(&mut self, value: T) {
        self.value.push(value);
    }
    pub fn pop(&mut self) -> Option<T> {
        self.value.pop()
    }
}
impl<T> Chunk<T> {
    pub fn new() -> Self {
        Self::default()
    }
    pub fn len(&self) -> usize {
        self.value.len()
    }
    pub fn get(&self, idx: usize) -> Option<&T> {
        self.value.get(idx)
    }
    pub fn iter(&self) -> slice::Iter<'_, T> {
        self.value.iter()
    }
    pub fn upgrade(self) -> FullChunk<T> {
        FullChunk {
            value: Ref::new(self.value.into_inner().ok().unwrap()),
        }
    }
    pub fn is_empty(&self) -> bool {
        self.value.is_empty()
    }
}
impl<T> FullChunk<T> {
    pub fn iter(&self) -> slice::Iter<'_, T> {
        self.value.iter()
    }
}

impl<T> FullChunk<T>
where
    T: Clone,
{
    pub fn downgrade(mut self) -> Chunk<T> {
        Ref::make_mut(&mut self.value);
        Chunk {
            value: ArrayVec::from(Ref::try_unwrap(self.value).ok().unwrap()),
        }
    }
}
impl<T> Clone for FullChunk<T> {
    fn clone(&self) -> Self {
        Self {
            value: self.value.clone(),
        }
    }
}
impl<T> Default for Chunk<T> {
    fn default() -> Self {
        Self {
            value: ArrayVec::default(),
        }
    }
}
impl<T> Index<usize> for Chunk<T> {
    type Output = T;
    fn index(&self, idx: usize) -> &Self::Output {
        &self.value[idx]
    }
}
impl<T> IndexMut<usize> for Chunk<T>
where
    T: Clone,
{
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        &mut self.value[idx]
    }
}

impl<T> Index<usize> for FullChunk<T> {
    type Output = T;
    fn index(&self, idx: usize) -> &Self::Output {
        &self.value[idx]
    }
}
impl<T> IndexMut<usize> for FullChunk<T>
where
    T: Clone,
{
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        &mut Ref::make_mut(&mut self.value)[idx]
    }
}
